var AWS = require('aws-sdk'),
  s3 = new AWS.S3(),
  s3Client = require('s3').createClient({s3Client: s3}),
  awsUpload = require('./aws-upload'),
  git = require('git-rev-sync');

module.exports = function (options) {
  var project = options.project,
    currentBranch = git.branch(process.cwd()),
    dirToUpload = options.dirToUpload,
    defaultBranches = ['master', 'release'],
    branchesList = options.branchesList || (process.env.AWS_BRANCHES) ? process.env.AWS_BRANCHES.split(',') : [],
    branches = defaultBranches.concat(branchesList),
    branchNameToBucketName = options.branchNameToBucketName || function (branch) {
        var parts = ['hp-ui', project];

        if (branch == 'master') {
          parts.push('dev');
        } else if (branch != 'release') {
          parts.push(branch.replace('feature/', '').toLowerCase());
        }

        return parts.join('-');
      },
    normalizeBucketName = options.normalizeBucketName || function (bucket) {
        var regex = new RegExp('^hp-ui-' + project + '-?'),
          name = bucket.replace(regex, '').toLowerCase();

        if (name == 'dev') {
          return 'master';
        } else if (name == '') {
          return 'release';
        }

        return name;
      },
    onEnd = options.onEnd || function () {
      };

  function throwIfError(err) {
    if (err) {
      throw err;
    }
  }

  function deleteBucket(bucket) {
    process.stdout.write('Deleting bucket ' + bucket + '... ');

    var promise = new Promise(function (resolve, reject) {
      var del = s3Client.deleteDir({
        Bucket: bucket,
        Prefix: '/'
      });

      del.on('error', function (err) {
        console.error('Unable to delete bucket ' + bucket, err);
        reject();
      });
      del.on('end', function () {
        s3.deleteBucket({Bucket: bucket}, function (err, data) {
          if (err) {
            console.error('Error while deleting bucket ' + bucket, err);
            reject();
          }
          else {
            console.log('Bucket ' + bucket + ' deleted.');
            resolve();
          }
        });
      });
    });

    return promise;
  }

  function syncBuckets() {
    console.log('Syncing buckets...');

    var promise = new Promise(function (resolve, reject) {
      s3.listBuckets(function (err, data) {
        throwIfError(err);

        var buckets = data.Buckets.filter(function (bucket) {
          return bucket.Name.indexOf('hp-ui-' + project) == 0;
        }).map(function (bucket) {
          return bucket.Name;
        });

        var allowedBuckets = branches.map(function (branch) {
          return branch.replace('feature/', '').toLowerCase();
        });

        var bucketsToDelete = buckets.filter(function (bucket) {
          var bucketName = normalizeBucketName(bucket);

          console.log('Testing if', bucketName, 'is allowed');

          return allowedBuckets.indexOf(bucketName) == -1;
        });

        console.log('Buckets: ', buckets);
        console.log('Branches: ', allowedBuckets);

        Promise.all(bucketsToDelete.map(deleteBucket))
          .then(resolve)
          .catch(reject);
      });
    });

    return promise;
  }

  function createBucket() {
    var bucket = branchNameToBucketName(currentBranch);

    console.log('Creating bucket', bucket);

    var request = s3.createBucket({Bucket: bucket}).promise();

    return request.then(function (data) {
      console.log(bucket + ' bucket created.');
    }).catch(e => {
      if (e.code !== 'BucketAlreadyOwnedByYou') {
        console.log('create bucket error');
        console.log(e);
        throw e;
      }
    });
  }

  function syncDir() {
    console.log('Starting upload...');

    var bucket = branchNameToBucketName(currentBranch);

    awsUpload({
      bucket: bucket,
      dir: dirToUpload
    });
  }

  // start deployment
  return syncBuckets().then(function () {
    if (branches.indexOf(currentBranch) > -1) {
      createBucket().then(syncDir);
    } else {
      console.log('Not uploading current branch (' + currentBranch + '), as it is not in the list:', branches);
    }
  });

};

Deployment tools
================

This is a helper repository, that helps you deploy your project to Amazon S3, it can be used with or without Gulp. The
tool will upload your project to a bucket. Bucket names are constructed the following way:

`hp-ui-{project}-{branch name}`

Not every branch will be uploaded, it depends on your settings, as you can see later.

There are two branches, that must be always present, as we have discussed, and behave a little different from the other 
branches. The branch `master` is used for development, therefore the suffix in this case will be `-dev` in the bucket
name. For the `release` branch, no suffix will be appended.

When you are working on a feature, you should do it in a feature branch. Usually we name it like
`feature/{short feature description}`, for example `feature/carousel`. These are renamed when uploading to S3, the
script will only use the name of the feature. For example the carousel feature branch in the Sustainability repository
would be uploaded to the bucket `hp-ui-sustainability-carousel`.

Unused buckets, those that no longer have branches will be automatically purged.

How can I use it?
-----------------

First, install it as an npm dependency:

```npm install --save https://bitbucket.org/hpCom-capabilities/deployment-tools.git```

Then integrate it in your build process. For example I had to create a new Gulp task for Sustainability:

```
gulp.task('deploy', ['default', 'gitinfo'], function (cb) {
  deploymentTools({
    project: 'sustainability',
    dirToUpload: 'dist',
    onEnd: cb
  });
});
```

This task has two dependencies, first build the project and then put a git.txt under the folder to be deployed. It will
be useful to see what was last deployed to S3. The `deploymentTools` function call can have a bunch of parameters, the
ones above are the most important ones.

* `project` - this will be inserted into the bucket name.
* `dirToUpload` - this folder will be uploaded under the root of the bucket.
* `cb` - it accepts a function as a parameter and it will be called once the upload is complete.

For the rest of the parameters, see the source code :)

To test the upload you can run your gulp task (or your build scripts) from the command line. You have to set up the
required environmental variables. 
 
```AWS_ACCESS_KEY_ID=... AWS_SECRET_ACCESS_KEY=... AWS_BRANCHES=feature/carousel,polymer gulp deploy```

This works with UNIX-like systems, not sure how you can run it from Windows' command line. Use your git-bash shell on
Windows.

* `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` are the credentials for Amanzon S3. If you don't have yours, ask
  Phil to send you
* `AWS_BRANCHES` defined which branches should be uploaded with a comma separated list. `master` and `release` are 
  uploaded by default, but you can also override this behavior.

Automatizing deployment with Bitbucket Pipelines
------------------------------------------------

First, go to your projects Bitbucket page and click on the Pipelines icon on the right menu, and click on the
"Set up Pipelines" button. You need to be an administrator of this repository to set up Pipelines.

Go to the settings of the project and click Pipelines, to see the settings page of Pipelines. Here you can set your 
environmental variables that can be used from the build scripts. You only have to define AWS_BRANCHES, because the 
credentials for S3 are defined at team level, and are accessible from the projects. If you have added it previously, 
you can safely remove AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY.)

Now you have to create a `bitbucket-pipelines.yml` file in your project's master branch. This will list the commands,
that must be run during build. For more information, see its
[documentation](https://confluence.atlassian.com/bitbucket/configure-bitbucket-pipelines-yml-792298910.html).

If you just want to upload your project to S3, you can use this as a template:

```
image: node:5.11.0

pipelines:
  default:
    - step:
        script:
          - npm install
          - npm install -g bower gulp
          - bower install --config.interactive=false --allow-root
          - gulp deploy
```

When you commit and push this file to Bitbucket, Pipelines will notice it, and start the build. It will spin up a new
Docker container, in this case `node:5.11.0`. Then it will execute the commands listed in the file in order.

There is a docker image that is prepared for the tasks we typically use. Currently it adds SSH keys for Bitbucket, and
caches some npm packages for faster `npm install`. To use this image, replace the first line of your pipelines 
configuration with the following:

```
image: balazsgabor/pipelines:latest
```

The source code for the image is available at [Bitbucket](https://bitbucket.org/hpCom-capabilities/docker-pipelines).

Later I will add Selenium with Firefox and Chrome for automated testing.

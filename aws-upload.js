var AWS = require('aws-sdk');
var mime = require('mime');
var path = require('path');
var fs = require('fs');
var _ = require('lodash');
var globby = require('globby');

module.exports = function (config) {
  // Set your region for future requests.
  AWS.config.region = config.region || 'us-east-1';

  var s3 = new AWS.S3({
    params: {
      Bucket: config.bucket || 'hp-ui-upload-test'
    }
  });

  function upload(params) {
    return new Promise(function (resolve, reject) {

      _.assign(params, {
        Key: params.target,
        ACL: 'public-read',
        Body: fs.createReadStream(params.source),
        ContentType: mime.lookup(params.source)
      });

      s3.upload(params, function (err, data) {
        if (err) {
          console.log('error');
          console.log(err);
          reject(err);
        } else {
          //console.log('success');
          console.log('file:', data.key);
          resolve(data);
        }
      });
    });
  }

  function fileList(dir) {
    var list = globby.sync(path.join(dir, '**', '*'))
    .map(function (item) {
      return {
        source: item,
        target: item
        .replace(dir, '')
        .slice(1)
      }
    })
    .filter(function (item) {
      return /\./.test(item.source)
    });
    
    console.log('fileList', list);
    return list;
  }


  function uploadList(files) {
    var list = [];
    _.forEach(files, function (item) {
      list.push(function () {
        upload(item);
        // .then(function (data) {
        //    //console.log('data', data);
        // })
      });
    });
    return list;
  }

  var final = uploadList(
    fileList(config.dir || 'dist')
  );

  //sequence mode
  _.reduce(final, function (sequence, current) {
    // Add these actions to the end of the sequence
    return sequence.then(current);
  }, Promise.resolve());

  return true;
}
